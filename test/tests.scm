(import (scheme base)
        (scheme write)
        (scheme file)
        (scheme eval)
        (retropikzel file-util v0-1-3 main)
        (retropikzel scheme-preprocessor v0-1-0 main))

(define env '((scheme base)
              (scheme write)
              (scheme read)))

(display (scheme-preprocessor-eval (file-util-slurp "test/test.spp") env))
