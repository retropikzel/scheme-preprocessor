;;; Copyright Joona "Retropikzel" Isoaho (2022-2023)
;;; This program is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Lesser General Public License as published by the
;;; Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
;;; License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public License
;;; along with this program. If not, see <https://www.gnu.org/licenses/>."

(define-library
  (retropikzel scheme-preprocessor v0.1.1 main)
  (import (scheme base)
          (scheme read)
          (scheme write)
          (scheme file)
          (scheme eval)
          (scheme process-context)
          (retropikzel file-util v0-1-3 main))
  (export scheme-preprocessor-eval
          scheme-preprocessor-include)
  (begin

    (define current-environment (environment))

    (define starts-with-opening-tag?
      (lambda (content)
        (and (> (length content) 5)
             (equal? (reverse (list-tail (reverse content) (- (length content) 5)))
                     (list #\< #\? #\s #\p #\p)))))

    (define starts-with-closing-tag?
      (lambda (content)
        (and (> (length content) 2)
             (equal? (reverse (list-tail (reverse content) (- (length content) 2)))
                     (list #\? #\>)))))

    (define eval->string
      (lambda (code-characters env)
        (let ((code (read (open-input-string (list->string code-characters)))))
          (parameterize
            ((current-output-port
               (open-output-string)))
            (eval code env)
            (get-output-string (current-output-port))))))

    (define scheme-preprocessor-process
      (lambda (content env previous-in-tag? code result)
        (if (null? content)
          (list->string result)
          (let* ((character (car content))
                 (new-in-tag? (cond ((starts-with-opening-tag? content) #t)
                                    ((starts-with-closing-tag? content) #f)
                                    (else previous-in-tag?)))
                 (new-content (cond ((and new-in-tag? (not previous-in-tag?))
                                     (cdr (cdr (cdr (cdr (cdr content))))))
                                    ((and previous-in-tag? (not new-in-tag?))
                                     (cdr (cdr content)))
                                    (else (cdr content))))
                 (new-code (cond ((and previous-in-tag? new-in-tag?)
                                  (append code (list character)))
                                 ((and previous-in-tag? (not new-in-tag?))
                                  (list))
                                 (else code)))
                 (new-result (cond ((and (not previous-in-tag?) (not new-in-tag?))
                                    (append result (list character)))
                                   ((and previous-in-tag? (not new-in-tag?))
                                    (append result (string->list (eval->string code env))))
                                   (else result))))
            (scheme-preprocessor-process new-content env new-in-tag? new-code new-result)))))

    (define scheme-preprocessor-eval
      (lambda (content env-libraries)
        (let ((env (apply environment (append env-libraries
                                              (list '(retropikzel scheme-preprocessor v0-1-0 main))))))
          (set! current-environment env)
          (scheme-preprocessor-process (string->list content) env #f (list) (list)))))

    (define scheme-preprocessor-include
      (lambda (filename)
        (display (scheme-preprocessor-process (string->list (file-util-slurp filename))
                                              current-environment
                                              #f
                                              (list)
                                              (list)))))))
