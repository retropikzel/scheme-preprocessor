(define-library
  (retropikzel file-util v0-1-3 main)
  (import (scheme base)
          (scheme read)
          (scheme write)
          (scheme file)
          (retropikzel string-util v0-1-0 main)
          (srfi 170))
  (export file-util-copy
          file-util-copy-directory
          file-util-ensure-directory-exists
          file-util-slurp)
  (begin

    (define file-util-ensure-directory-exists
      (lambda (directory)
        (letrec ((split-path (string-util-split-by-char directory #\/))
                 (looper (lambda (args path)
                           (if (and (not (string=? path ""))
                                    (not (file-exists? path)))
                             (create-directory path))
                           (if (not (null? args))
                             (looper (cdr args) (string-append path "/" (car args)))))))
          (if (not (null? split-path))
            (looper (cdr split-path) (car split-path))))))

    (define file-util-copy
      (lambda (source-path destination-path)
        (let ((source-port (open-binary-input-file source-path))
              (destination-port (open-binary-output-file destination-path)))
          (letrec ((loop (lambda (bytes)
                           (if (not (eof-object? bytes))
                             (begin
                               (write-bytevector bytes
                                                 destination-port)
                               (loop (read-bytevector 20000 source-port)))))))
            (loop (read-bytevector 20000 source-port)))
          (close-port source-port)
          (close-port destination-port))))

    (define file-util-copy-directory
      (lambda (from to)
        (file-util-ensure-directory-exists to)
        (map
          (lambda (file)
            (let* ((from-path (string-append from "/" file))
                   (to-path (string-append to "/" file))
                   (is-directory? (file-info-directory? (file-info from-path #f))))
              (if is-directory?
                (begin
                  (file-util-ensure-directory-exists to-path)
                  (file-util-copy-directory from-path to-path))
                (begin
                  (file-util-copy from-path to-path)))))
          (directory-files from))))

    (define file-util-slurp
      (lambda (file)
        (utf8->string
          (with-input-from-file
            file
            (lambda ()
              (letrec ((looper (lambda (result)
                                 (let ((bytes (read-bytevector 4000)))
                                   (if (eof-object? bytes)
                                     result
                                     (looper (bytevector-append result bytes)))))))
                (looper (bytevector))))))))))
