guix-shell:
	guix shell gauche time

tmp:
	mkdir -p tmp

run-tests: tmp
	time gosh -I . -I ./schubert test/tests.scm
